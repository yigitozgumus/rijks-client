package com.example.ozgumusy.rijksclient.jsonmodels;

/**
 * Created by ozgumusy on 27/07/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Maker {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("unFixedName")
    @Expose
    private String unFixedName;
    @SerializedName("placeOfBirth")
    @Expose
    private String placeOfBirth;
    @SerializedName("dateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("dateOfBirthPrecision")
    @Expose
    private Object dateOfBirthPrecision;
    @SerializedName("dateOfDeath")
    @Expose
    private String dateOfDeath;
    @SerializedName("dateOfDeathPrecision")
    @Expose
    private Object dateOfDeathPrecision;
    @SerializedName("placeOfDeath")
    @Expose
    private String placeOfDeath;
    @SerializedName("occupation")
    @Expose
    private List<String> occupation = new ArrayList<String>();
    @SerializedName("roles")
    @Expose
    private List<Object> roles = new ArrayList<Object>();
    @SerializedName("nationality")
    @Expose
    private String nationality;
    @SerializedName("biography")
    @Expose
    private Object biography;
    @SerializedName("productionPlaces")
    @Expose
    private List<Object> productionPlaces = new ArrayList<Object>();
    @SerializedName("qualification")
    @Expose
    private String qualification;

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return
     * The unFixedName
     */
    public String getUnFixedName() {
        return unFixedName;
    }

    /**
     *
     * @return
     * The placeOfBirth
     */
    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    /**
     *
     * @return
     * The dateOfBirth
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     *
     * @return
     * The dateOfBirthPrecision
     */
    public Object getDateOfBirthPrecision() {
        return dateOfBirthPrecision;
    }


    /**
     *
     * @return
     * The dateOfDeath
     */
    public String getDateOfDeath() {
        return dateOfDeath;
    }

    /**
     *
     * @return
     * The dateOfDeathPrecision
     */
    public Object getDateOfDeathPrecision() {
        return dateOfDeathPrecision;
    }


    /**
     *
     * @return
     * The placeOfDeath
     */
    public String getPlaceOfDeath() {
        return placeOfDeath;
    }

    /**
     *
     * @return
     * The occupation
     */
    public List<String> getOccupation() {
        return occupation;
    }

    /**
     *
     * @return
     * The roles
     */
    public List<Object> getRoles() {
        return roles;
    }

    /**
     *
     * @return
     * The nationality
     */
    public String getNationality() {
        return nationality;
    }

    /**
     *
     * @return
     * The biography
     */
    public Object getBiography() {
        return biography;
    }

    /**
     *
     * @return
     * The productionPlaces
     */
    public List<Object> getProductionPlaces() {
        return productionPlaces;
    }


    /**
     *
     * @return
     * The qualification
     */
    public String getQualification() {
        return qualification;
    }


}