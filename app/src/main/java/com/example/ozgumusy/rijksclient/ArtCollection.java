package com.example.ozgumusy.rijksclient;

/**
 * Created by ozgumusy on 26/07/2016.
 */
public class ArtCollection {

    //Data Fields of the Collection
    private String webLink;
    private String id;
    private String objectNum;
    private String title;
    private String headerImageUrl;
    private String webImageUrl;


    public String getWebLink() {
        return webLink;
    }

    public void setWebLink(String webLink) {
        this.webLink = webLink;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHeaderImageUrl() {
        return headerImageUrl;
    }

    public void setHeaderImageUrl(String headerImage) {
        this.headerImageUrl = headerImage;
    }

    public String getWebImageUrl() {
        return webImageUrl;
    }

    public void setWebImageUrl(String webImage) {
        this.webImageUrl = webImage;
    }

    public String getObjectNum() {
        return objectNum;
    }

    public void setObjectNum(String objectNum) {
        this.objectNum = objectNum;
    }
}
