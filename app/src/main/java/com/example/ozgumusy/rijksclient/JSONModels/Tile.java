package com.example.ozgumusy.rijksclient.jsonmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Tile {

    @SerializedName("x")
    @Expose
    private Integer x;
    @SerializedName("y")
    @Expose
    private Integer y;
    @SerializedName("url")
    @Expose
    private String url;

    /**
     *
     * @return
     * The x
     */
    public Integer getX() {
        return x;
    }

    /**
     *
     * @return
     * The y
     */
    public Integer getY() {
        return y;
    }


    /**
     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

}