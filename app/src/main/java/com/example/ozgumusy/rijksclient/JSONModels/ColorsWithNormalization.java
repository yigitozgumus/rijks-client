package com.example.ozgumusy.rijksclient.jsonmodels;

/**
 * Created by ozgumusy on 21/07/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ColorsWithNormalization {

    @SerializedName("originalHex")
    @Expose
    private String originalHex;
    @SerializedName("normalizedHex")
    @Expose
    private String normalizedHex;

    /**
     *
     * @return
     * The originalHex
     */
    public String getOriginalHex() {
        return originalHex;
    }

    /**
     *
     * @return
     * The normalizedHex
     */
    public String getNormalizedHex() {
        return normalizedHex;
    }

}
