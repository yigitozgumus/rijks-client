package com.example.ozgumusy.rijksclient.network;

import com.example.ozgumusy.rijksclient.jsonmodels.TranslatedText;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by ozgumusy on 27/07/2016.
 */
public interface TranslaterAPI {
    //Create the endpoint
    String ENDPOINT = "https://www.googleapis.com/";

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(TranslaterAPI.ENDPOINT)
            .addConverterFactory(GsonConverterFactory.create())
            .build();


    @GET("language/translate/v2")
    Call<TranslatedText> getTranslation(@QueryMap Map<String, String> options);
}
