package com.example.ozgumusy.rijksclient.network;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ozgumusy on 09/08/2016.
 */
public class QueryWrapper {

    public Map<String, String> getOptions() {
        return options;
    }


    private Map<String, String> options;
    //Default constructor
    public QueryWrapper(){
        options = new HashMap<>();
        options.put("format","json");
        options.put("key","uvB5vrkP");
    }
    //Constructor for the query
    public QueryWrapper(int resultNum,String query){
        options = new HashMap<>();
        options.put("ps",String.valueOf(resultNum));
        options.put("q",query);
        options.put("format","json");
        options.put("key","uvB5vrkP");
    }


}
