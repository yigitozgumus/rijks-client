package com.example.ozgumusy.rijksclient.jsonmodels;

/**
 * Created by ozgumusy on 21/07/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class ArtObject {

    @SerializedName("links")
    @Expose
    private Links links;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("objectNumber")
    @Expose
    private String objectNumber;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("hasImage")
    @Expose
    private Boolean hasImage;
    @SerializedName("principalOrFirstMaker")
    @Expose
    private String principalOrFirstMaker;
    @SerializedName("longTitle")
    @Expose
    private String longTitle;
    @SerializedName("showImage")
    @Expose
    private Boolean showImage;
    @SerializedName("permitDownload")
    @Expose
    private Boolean permitDownload;
    @SerializedName("webImage")
    @Expose
    private WebImage webImage;
    @SerializedName("headerImage")
    @Expose
    private HeaderImage headerImage;
    @SerializedName("productionPlaces")
    @Expose
    private List<Object> productionPlaces = new ArrayList<Object>();

    public Links getLinks() {
        return links;
    }


    public String getId() {
        return id;
    }

    public String getObjectNumber() {
        return objectNumber;
    }

    public void setObjectNumber(String objectNumber) {
        this.objectNumber = objectNumber;
    }

    public String getTitle() {
        return title;
    }


    public Boolean getHasImage() {
        return hasImage;
    }


    public String getPrincipalOrFirstMaker() {
        return principalOrFirstMaker;
    }


    public String getLongTitle() {
        return longTitle;
    }

    public Boolean getShowImage() {
        return showImage;
    }


    public Boolean getPermitDownload() {
        return permitDownload;
    }


    public WebImage getWebImage() {
        return webImage;
    }


    public HeaderImage getHeaderImage() {
        return headerImage;
    }


    public List<Object> getProductionPlaces() {
        return productionPlaces;
    }

}