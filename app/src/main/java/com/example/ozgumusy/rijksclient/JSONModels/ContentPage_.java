package com.example.ozgumusy.rijksclient.jsonmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class ContentPage_ {

    @SerializedName("thumbnailLandscape")
    @Expose
    private ThumbnailLandscape thumbnailLandscape;
    @SerializedName("inOverview")
    @Expose
    private Boolean inOverview;
    @SerializedName("isHighlightedOnOverview")
    @Expose
    private Boolean isHighlightedOnOverview;
    @SerializedName("artistBio")
    @Expose
    private Object artistBio;
    @SerializedName("maker")
    @Expose
    private Maker maker;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("callToAction")
    @Expose
    private String callToAction;
    @SerializedName("callToActionQuery")
    @Expose
    private String callToActionQuery;
    @SerializedName("artObjectSet")
    @Expose
    private List<String> artObjectSet = new ArrayList<String>();
    @SerializedName("mediaBlocks")
    @Expose
    private List<Object> mediaBlocks = new ArrayList<Object>();
    @SerializedName("artobjects_on_this_page")
    @Expose
    private List<String> artobjectsOnThisPage = new ArrayList<String>();
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("guid")
    @Expose
    private String guid;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("compactHeader")
    @Expose
    private Boolean compactHeader;
    @SerializedName("shortcutKeywords")
    @Expose
    private List<Object> shortcutKeywords = new ArrayList<Object>();
    @SerializedName("otherLangs")
    @Expose
    private List<String> otherLangs = new ArrayList<String>();
    @SerializedName("headerImage")
    @Expose
    private String headerImage;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("body")
    @Expose
    private Body body;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("updatedOn")
    @Expose
    private String updatedOn;

    /**
     *
     * @return
     * The thumbnailLandscape
     */
    public ThumbnailLandscape getThumbnailLandscape() {
        return thumbnailLandscape;
    }



    /**
     *
     * @return
     * The inOverview
     */
    public Boolean getInOverview() {
        return inOverview;
    }


    /**
     *
     * @return
     * The isHighlightedOnOverview
     */
    public Boolean getIsHighlightedOnOverview() {
        return isHighlightedOnOverview;
    }


    /**
     *
     * @return
     * The artistBio
     */
    public Object getArtistBio() {
        return artistBio;
    }


    /**
     *
     * @return
     * The maker
     */
    public Maker getMaker() {
        return maker;
    }


    /**
     *
     * @return
     * The subject
     */
    public String getSubject() {
        return subject;
    }


    /**
     *
     * @return
     * The callToAction
     */
    public String getCallToAction() {
        return callToAction;
    }

    /**
     *
     * @return
     * The callToActionQuery
     */
    public String getCallToActionQuery() {
        return callToActionQuery;
    }


    /**
     *
     * @return
     * The artObjectSet
     */
    public List<String> getArtObjectSet() {
        return artObjectSet;
    }

    /**
     *
     * @return
     * The mediaBlocks
     */
    public List<Object> getMediaBlocks() {
        return mediaBlocks;
    }


    /**
     *
     * @return
     * The artobjectsOnThisPage
     */
    public List<String> getArtobjectsOnThisPage() {
        return artobjectsOnThisPage;
    }


    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }


    /**
     *
     * @return
     * The guid
     */
    public String getGuid() {
        return guid;
    }

    /**
     *
     * @return
     * The lang
     */
    public String getLang() {
        return lang;
    }

    /**
     *
     * @return
     * The compactHeader
     */
    public Boolean getCompactHeader() {
        return compactHeader;
    }

    /**
     *
     * @return
     * The shortcutKeywords
     */
    public List<Object> getShortcutKeywords() {
        return shortcutKeywords;
    }

    /**
     *
     * @return
     * The otherLangs
     */
    public List<String> getOtherLangs() {
        return otherLangs;
    }

    /**
     *
     * @return
     * The headerImage
     */
    public String getHeaderImage() {
        return headerImage;
    }

    /**
     *
     * @return
     * The thumbnail
     */
    public String getThumbnail() {
        return thumbnail;
    }

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @return
     * The description
     */
    public Object getDescription() {
        return description;
    }

    /**
     *
     * @return
     * The body
     */
    public Body getBody() {
        return body;
    }

    /**
     *
     * @return
     * The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     *
     * @return
     * The updatedOn
     */
    public String getUpdatedOn() {
        return updatedOn;
    }

}