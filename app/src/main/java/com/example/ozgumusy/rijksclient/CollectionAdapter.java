package com.example.ozgumusy.rijksclient;

/**
 * Created by ozgumusy on 26/07/2016.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
public class CollectionAdapter extends RecyclerView.Adapter<CollectionAdapter.ViewHolder> {
    private ArrayList<ArtCollection> artCollections;
    private Context context;

    private static ClickListener clickListener;

    //Creating onClick listener for the ImageTexts
    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, View.OnLongClickListener {
        ImageView imgCollection;
        public ViewHolder(View view) {
            super(view);
            imgCollection = (ImageView)view.findViewById(R.id.collection_img);
            imgCollection.setOnClickListener(this);
            imgCollection.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return false;
        }

    }
    public void setOnItemClickListener(ClickListener clickListener) {
        CollectionAdapter.clickListener = clickListener;
    }


    // Constructor of the adapter
    public CollectionAdapter(Context context,ArrayList<ArtCollection> artCollections){
        this.context = context;
        this.artCollections = artCollections;
    }


    @Override
    public CollectionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                                  .inflate(R.layout.collection_row,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        holder.collectionTitle.setText(artCollections.get(position).getTitle());
        Picasso.with(context)
                .load(artCollections.get(position).getWebImageUrl())
                .fit()
                .placeholder(R.drawable.rijksmuseum)
                .into(holder.imgCollection);

    }
    @Override
    public int getItemCount() {
        return artCollections.size();
    }


}
