package com.example.ozgumusy.rijksclient.jsonmodels;

/**
 * Created by ozgumusy on 21/07/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class HeaderImage {

    @SerializedName("guid")
    @Expose
    private String guid;
    @SerializedName("offsetPercentageX")
    @Expose
    private Integer offsetPercentageX;
    @SerializedName("offsetPercentageY")
    @Expose
    private Integer offsetPercentageY;
    @SerializedName("width")
    @Expose
    private Integer width;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("url")
    @Expose
    private String url;

    /**
     *
     * @return
     * The guid
     */
    public String getGuid() {
        return guid;
    }

    /**
     *
     * @return
     * The offsetPercentageX
     */
    public Integer getOffsetPercentageX() {
        return offsetPercentageX;
    }


    /**
     *
     * @return
     * The offsetPercentageY
     */
    public Integer getOffsetPercentageY() {
        return offsetPercentageY;
    }


    /**
     *
     * @return
     * The width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     *
     * @return
     * The height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

}