package com.example.ozgumusy.rijksclient.jsonmodels;

/**
 * Created by ozgumusy on 21/07/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Label {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("makerLine")
    @Expose
    private String makerLine;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("notes")
    @Expose
    private Object notes;
    @SerializedName("date")
    @Expose
    private String date;

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @return
     * The makerLine
     */
    public String getMakerLine() {
        return makerLine;
    }

    /**
     *
     * @return
     * The description
     */
    public Object getDescription() {
        return description;
    }

    /**
     *
     * @return
     * The notes
     */
    public Object getNotes() {
        return notes;
    }

    /**
     *
     * @return
     * The date
     */
    public String getDate() {
        return date;
    }

}
