package com.example.ozgumusy.rijksclient.jsonmodels;

/**
 * Created by ozgumusy on 21/07/2016.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Acquisition {

    @SerializedName("method")
    @Expose
    private Object method;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("creditLine")
    @Expose
    private String creditLine;

    /**
     *
     * @return
     * The method
     */
    public Object getMethod() {
        return method;
    }


    /**
     *
     * @return
     * The date
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @return
     * The creditLine
     */
    public String getCreditLine() {
        return creditLine;
    }


}
