package com.example.ozgumusy.rijksclient.jsonmodels;

/**
 * Created by ozgumusy on 28/07/2016.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Translation {

    @SerializedName("translatedText")
    @Expose
    private String translatedText;

    /**
     *
     * @return
     * The translatedText
     */
    public String getTranslatedText() {
        return translatedText;
    }

}