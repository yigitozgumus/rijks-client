package com.example.ozgumusy.rijksclient.jsonmodels;

/**
 * Created by ozgumusy on 21/07/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class ArtContent  {

    @SerializedName("elapsedMilliseconds")
    @Expose
    private Integer elapsedMilliseconds;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("artObjects")
    @Expose
    private List<ArtObject> artObjects = new ArrayList<>();

    /**
     *
     * @return
     * The elapsedMilliseconds
     */
    public Integer getElapsedMilliseconds() {
        return elapsedMilliseconds;
    }


    /**
     *
     * @return
     * The count
     */
    public Integer getCount() {
        return count;
    }


    /**
     *
     * @return
     * The artObjects
     */
    public List<ArtObject> getArtObjects() {
        return artObjects;
    }

}