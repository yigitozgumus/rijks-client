package com.example.ozgumusy.rijksclient.activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.edmodo.cropper.CropImageView;
import com.example.ozgumusy.rijksclient.ArtCollectionDetail;
import com.example.ozgumusy.rijksclient.ArtContentDetailed;
import com.example.ozgumusy.rijksclient.R;
import com.example.ozgumusy.rijksclient.features.CropImageActivity;
import com.example.ozgumusy.rijksclient.jsonmodels.ArtContent;
import com.example.ozgumusy.rijksclient.jsonmodels.ArtObjectDetailed;
import com.example.ozgumusy.rijksclient.jsonmodels.TileSet;
import com.example.ozgumusy.rijksclient.jsonmodels.TranslatedText;
import com.example.ozgumusy.rijksclient.network.CollectionAPI;
import com.example.ozgumusy.rijksclient.network.QueryWrapper;
import com.example.ozgumusy.rijksclient.network.TranslaterAPI;
import com.google.gson.Gson;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.Eases.EaseType;
import com.nightonke.boommenu.Types.BoomType;
import com.nightonke.boommenu.Types.ButtonType;
import com.nightonke.boommenu.Types.ClickEffectType;
import com.nightonke.boommenu.Types.PlaceType;
import com.nightonke.boommenu.Util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CollectionPageActivity extends AppCompatActivity
    implements BoomMenuButton.OnSubButtonClickListener{

    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 666;
    private static final int RESULT_SETTINGS = 1;
    public static final String COLLECTION_MESSAGE = "com.example.ozgumusy.rijksclient.MESSAGE";

    private ArtContentDetailed DetailedCollection;
    private ArtCollectionDetail Detailed;
    private TileSet tiles ;
    private WallpaperManager wallpaperManager;
    private boolean isQueryVisible = false;
    private CropImageView resultView ;
    private BoomMenuButton boomButton;
    private boolean isInit = false;
    private Context mContext;
    private ArrayList<Integer> colorPallette ;

    private Bitmap bmp;

    protected String urlLink ;
    protected boolean shouldTranslate = true ;
    protected Palette myPalette ;
    protected com.getbase.floatingactionbutton.FloatingActionButton wallpaperSet ;
    protected com.getbase.floatingactionbutton.FloatingActionButton favouriteSet;
    protected com.getbase.floatingactionbutton.FloatingActionButton shareSet;
    protected com.getbase.floatingactionbutton.FloatingActionButton saveSet;
    protected ImageView pageImage;

    AutoCompleteTextView queryTextView;


    private String imagePath ;

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    private boolean favourite = false;

    public void setQueryVisible(boolean queryVisible) {
        isQueryVisible = queryVisible;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_collection_page);
        queryTextView = (AutoCompleteTextView) findViewById(R.id.query_edit2);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_expandable_list_item_1, MainActivity.queries);
        queryTextView.setAdapter(adapter);
        queryTextView.setThreshold(1);

        //Get the intent info
        Intent intent = getIntent();
        String objectNumber = intent.getStringExtra(CollectionsActivity.OBJECT_MESSAGE);
        urlLink = intent.getStringExtra("link");
        // Get the Data from the API
        CollectionAPI colApi = CollectionAPI.retrofitEn.create(CollectionAPI.class);
        QueryWrapper data = new QueryWrapper();
        try {
            getCollectionInfo(objectNumber,data.getOptions(),colApi);
        } catch (IOException e) {
            e.printStackTrace();
        }
        getTilesInfo(urlLink,data.getOptions(),colApi);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);
        pageImage = (ImageView) findViewById(R.id.imgCollection);
        boomButton = (BoomMenuButton)findViewById(R.id.boom);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        favouriteSet = (com.getbase.floatingactionbutton.FloatingActionButton) findViewById(R.id.favourite_button);
        favouriteSet.setIcon(R.drawable.ic_star_border_black_18dp);
        favouriteSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modifyFavourites(favourite);
                }
            });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (!isInit) {
            initBoom();
        }
        isInit = true;
    }

    private void initBoom() {
        int number = 3;

        Drawable[] drawables = new Drawable[number];
        int[] drawablesResource = new int[]{
                R.drawable.ic_share_black_18dp,
                R.drawable.ic_save_black_18dp,
                R.drawable.ic_wallpaper_black_18dp
        };
        for (int i = 0; i < number; i++)
            drawables[i] = ContextCompat.getDrawable(mContext, drawablesResource[i]);

        String[] STRINGS = new String[]{
                "Share",
                "Export to Device",
                "Set as Wallpaper"
        };
        String[] strings = new String[number];
        for (int i = 0; i < number; i++)
            strings[i] = STRINGS[i];

        int[][] colors = new int[number][2];
        for (int i = 0; i < number; i++) {
            colors[i][1] = R.color.accent;
            colors[i][0] = Util.getInstance().getPressedColor(colors[i][1]);
        }

        ButtonType buttonType = ButtonType.HAM;

        // Now with Builder, you can init BMB more convenient
        new BoomMenuButton.Builder()
                .subButtons(drawables, colors, strings)
                .button(buttonType)
                .boom(BoomType.LINE)
                .place(PlaceType.HAM_3_1)
                .clickEffect(ClickEffectType.RIPPLE)
                .showMoveEase(EaseType.EaseOutBack)
                .hideMoveEase(EaseType.EaseOutCirc)
                .showScaleEase(EaseType.EaseOutBack)
                .hideScaleType(EaseType.EaseOutCirc)
                .boomButtonShadow(Util.getInstance().dp2px(2), Util.getInstance().dp2px(2))
                .subButtonsShadow(Util.getInstance().dp2px(2), Util.getInstance().dp2px(2))
                .onSubButtonClick(this)
                .init(boomButton);

    }

    private void checkStoragePermission() throws IOException{
        if(ContextCompat.checkSelfPermission(CollectionPageActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(CollectionPageActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            }else{
                ActivityCompat.requestPermissions(CollectionPageActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            }
        }
    }

    private void saveImagetoDevice() throws IOException{
          URL  url = new URL(Detailed.getWebUrl());

        Bitmap  bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        File sdCardDirectory =new File( Environment.getExternalStorageDirectory(),"savedImages" ) ;
        if(!sdCardDirectory.exists()){
            sdCardDirectory.mkdirs();
        }
        Random rand = new Random();

        File image = new File(sdCardDirectory,"image-" + String.valueOf(rand.nextInt(10000)));
        boolean success = false;
        FileOutputStream outStream ;
        try{
            outStream = new FileOutputStream(image);
            bitmap.compress(Bitmap.CompressFormat.PNG,100,outStream);
            outStream.flush();
            outStream.close();
            success= true;
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }

        if (success) {
            Toast.makeText(getApplicationContext(), image.getName() +" is saved with success to "
                    + sdCardDirectory.getAbsolutePath(),
                    Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Error during image saving", Toast.LENGTH_LONG).show();
        }
    }

    private void attemptImageToDevice() throws MalformedURLException,IOException {
        checkStoragePermission();
        saveImagetoDevice();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

                }
        }
    }

    public void shareIt() throws IOException {

        // Get access to bitmap image from view
        ImageView ivImage = (ImageView) findViewById(R.id.imgCollection);
        // Get access to the URI for the bitmap
        Uri bmpUri = getLocalBitmapUri(ivImage);

        if (bmpUri != null) {
            // Construct a ShareIntent with link to image
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
            shareIntent.setType("image/*");
            // Launch sharing dialog for image
            startActivity(Intent.createChooser(shareIntent, "Share Using"));
        } else {

        }
    }

    // Returns the URI path to the Bitmap displayed in specified ImageView
    public Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawaURL url = new URL(Detailed.getWebUrl());
        Uri bmpUri = null;
        try {
            // Use methods on Context to access package-specific directories on external storage.
            // This way, you don't need to request external read/write permission.
            // See https://youtu.be/5xVh-7ywKpE?t=25m25s
            File file =  new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    private void modifyFavourites(boolean favourite) {
        //TODO Figure out
        if(!favourite) {
            favouriteSet.setIcon(R.drawable.ic_star_black_18dp);
            Toast.makeText(this,R.string.favourite_add,Toast.LENGTH_LONG).show();
            setFavourite(true);
        }else{
            favouriteSet.setIcon(R.drawable.ic_star_border_black_18dp);
            Toast.makeText(this,R.string.favourite_remove,Toast.LENGTH_LONG).show();
            setFavourite(false);
        }

    }

    private class ColorPallette extends AsyncTask<URL,Void,Integer>{

        @Override
        protected Integer doInBackground(URL... urls) {
            Bitmap bitmap = null;
            colorPallette = new ArrayList<>();
                try {
                    URL test = urls[0];
                    bitmap = BitmapFactory.decodeStream(urls[0].openConnection().getInputStream());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Palette.from(bitmap).maximumColorCount(16).generate(palette -> {
                        List<Palette.Swatch> swatches = palette.getSwatches();
                        for (int i = 0; i <swatches.size() ; i++) {
                            colorPallette.add(swatches.get(i).getRgb());
                        }
                    });
            return 1;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            //TODO solve null pointer exception
//            LinearLayout view = (LinearLayout) findViewById(R.id.collection_page);
//            view.setBackgroundColor(colorPallette.get(integer));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_page, menu);
        return true;

    }

    private void transferUserSettings() {
        SharedPreferences prefs = PreferenceManager.
                getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("number_of_searches", String.valueOf(MainActivity.resultNum));
        editor.putString("language_choice",MainActivity.languageDefault);
        editor.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_SETTINGS:
                //updateUserSettings();
                break;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings2:
                // User chose the "Settings" item, show the app settings UI...
                Intent intent = new Intent(this,SettingsActivity.class);
                transferUserSettings();
                startActivityForResult(intent,RESULT_SETTINGS);
                return true;

            case R.id.action_search2:
                EditText editText = (EditText) findViewById(R.id.query_edit2);
                String query = editText.getText().toString();
                if(!isQueryVisible){
                    View myView = findViewById(R.id.query_edit2);
                    int cx = myView.getWidth()/2;
                    int cy = myView.getHeight()/2;
                    float finalRadius = (float) Math.hypot(cx,cy);
                    Animator anim = ViewAnimationUtils
                            .createCircularReveal(myView,cx,cy,0,finalRadius);
                    myView.setVisibility(View.VISIBLE);
                    anim.start();
                    setQueryVisible(true);
                }else if(isQueryVisible && query.equals("")){
                    View myView = findViewById(R.id.query_edit2);
                    int cx = myView.getWidth()/2;
                    int cy = myView.getHeight()/2;
                    float initialRadius = (float) Math.hypot(cx,cy);
                    Animator anim = ViewAnimationUtils
                            .createCircularReveal(myView,cx,cy,initialRadius,0);
                    anim.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            myView.setVisibility(View.INVISIBLE);
                        }
                    });
                    anim.start();
                    setQueryVisible(false);
                }else{
                    QueryWrapper data = new QueryWrapper(MainActivity.resultNum,query);
                    makeQuery(data.getOptions());
                }
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    public void makeQuery(Map<String,String> data) {
        Intent intent = new Intent(this,CollectionsActivity.class);
        //Get the query Text
        final StringBuilder[] test = {new StringBuilder()};
        // Api Call Initializations
        CollectionAPI colApi = CollectionAPI.retrofitEn.create(CollectionAPI.class);

        // data.put("type","painting");
        Call<ArtContent> callNew = colApi.getCollections(data);
        System.out.println(callNew.request().url().toString());
        callNew.enqueue(new Callback<ArtContent>() {
            @Override
            public void onResponse(Call<ArtContent> call, Response<ArtContent> response) {
                Gson gson = new Gson();
                test[0] = new StringBuilder(gson.toJson(response.body()).replace("\\u003ds0",""));
                System.out.println("Main screen inside mq response "+ test[0]);
                //Get the query Text
                intent.putExtra(COLLECTION_MESSAGE, test[0].toString());
                startActivity(intent);
            }
            @Override
            public void onFailure(Call<ArtContent> call, Throwable t) {
                System.out.println(t.getCause());
            }
        });

    }

    public void getCollectionInfo(String objectNumber,
                                  Map<String, String> options,
                                  CollectionAPI colApi) throws IOException{

        Call<ArtContentDetailed> call = colApi.getCollectionDetal(objectNumber,options);
        System.out.println(call.request().url().toString());
        call.enqueue(new Callback<ArtContentDetailed>() {
            @Override
            public void onResponse(Call<ArtContentDetailed> call,
                                   Response<ArtContentDetailed> response) {
                if(response.isSuccessful()){
                    DetailedCollection = response.body();
                    SharedPreferences prefs = PreferenceManager
                            .getDefaultSharedPreferences(getApplicationContext());
                    String language = prefs.getString("language_choice","NULL");
                    if(language.equals("en")){
                        try {
                            initViews(false); //TODO Change to true
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }else{
                        try {
                            initViews(false);
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }
                    try {
                        bmp = BitmapFactory.decodeStream(new URL(Detailed.getWebUrl())
                                .openConnection()
                                .getInputStream());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ArtContentDetailed> call, Throwable t) {

            }
        });
    }

    public void getTilesInfo(String objectNumber,
                             Map<String, String> options,
                             CollectionAPI colApi){
        Call<TileSet> tileSetCall = colApi.getTilesOfCollection(objectNumber,options);
        tileSetCall.enqueue(new Callback<TileSet>() {
            @Override
            public void onResponse(Call<TileSet> call,
                                   Response<TileSet> response) {
                if (response.isSuccessful()){
                    tiles = response.body();
                    System.out.println(tiles.toString());
                }
            }

            @Override
            public void onFailure(Call<TileSet> call, Throwable t) {

            }
        });
    }

    private ArtCollectionDetail preprocessData(){
        //Create the ArtCollectionDetail object
        ArtCollectionDetail artDet = new ArtCollectionDetail();
        //Create the getter object
        ArtObjectDetailed contents = DetailedCollection.getArtObject();
        //Transfer the necessary info
        artDet.setTitle(contents.getTitle());
        if(contents.getWebImage() != null){
            artDet.setWebUrl(contents.getWebImage().getUrl());
        }else{
            artDet.setWebUrl("http://corporate.kpn.com/upload_mm/2/0/8/cid3192_Teaser-logo-Rijks_700x449.jpg");
        }
        if(contents.getColors() != null) {
            artDet.setColorPalette(contents.getColors());
        }
        artDet.setTitles(contents.getTitles());
        if(contents.getNormalized32Colors() != null) {
            artDet.setNormalized32Colors(contents.getColorsWithNormalization());
        }else{
            artDet.setNormalized32Colors(contents.getColorsWithNormalization());
        }
        artDet.setCollectionURL(contents.getLinks().getWeb());
        if(contents.getDescription() !=null) {
            artDet.setDescription(contents.getDescription());
        }else{
            artDet.setDescription("There is no available Description");
        }
        artDet.setPrincipalMaker(contents.getPrincipalMakers());

        return artDet;
    }

    private void initViews(boolean translation) throws MalformedURLException {
        //Preprocess data
        Detailed = preprocessData();
        // Initialize the View objects
        ImageView imageView = (ImageView) findViewById(R.id.imgCollection);
        TextView titleView = (TextView) findViewById(R.id.collection_title);
        TextView makerView = (TextView) findViewById(R.id.maker);
        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        URL url = new URL(Detailed.getWebUrl());
        new ColorPallette().execute(url);
        // Add the content
        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(imageView);
        Glide.with(getApplicationContext()).load(Detailed.getWebUrl()).into(imageViewTarget);
        if (Detailed.getTitle() != ""){
            assert titleView != null;
            titleView.setText(Detailed.getTitle());
        }else{
            assert titleView != null;
            titleView.setText(R.string.no_title);
        }
        if(!Detailed.getPrincipalMaker().isEmpty()) if (makerView != null) {
            makerView.setText(Detailed.getPrincipalMaker().get(0).getName());
        }
        //Translate description and add to Textview
        if (translation){
            translate(Detailed.getDescription().toString());
        }else{
            TextView descpView = (TextView) findViewById(R.id.collection_descp);
            assert descpView != null;
            descpView.setText( Detailed.getDescription().toString());
        }

    }

    private void translate(String target){

        //Initialize the translater API object and create the retrofit object
        TranslaterAPI translaterAPI = TranslaterAPI.retrofit.create(TranslaterAPI.class);
        //Dirty hack but Android Studio suggested it so...
        TextView descpView = (TextView) findViewById(R.id.collection_descp);
        final String[] translatedText = new String[1];
        // Map the query parameters
        Map<String, String> options = new HashMap<>();
        options.put("key","AIzaSyCW-JWSz2k-g9PC8soN2iqi_vfhzTsqGhE");
        options.put("target","en");
        options.put("source","nl");
        options.put("q",target);
        // Make the api call with the getTranslation
        Call<TranslatedText> call2 = translaterAPI.getTranslation(options);
        System.out.println(call2.request().url().toString());
        // Call Asynchronously
        call2.enqueue(new Callback<TranslatedText>() {
            @Override
            public void onResponse(Call<TranslatedText> call, Response<TranslatedText> response) {
                if (response.isSuccessful()) {
                    if (descpView != null) {
                        descpView.setText(response.body()
                                .getData()
                                .getTranslations()
                                .get(0)
                                .getTranslatedText());
                    };
                }else{
                    System.out.println(response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<TranslatedText> call, Throwable t) {
                System.out.println(t.getCause().toString());
            }
        });

    }

    public void goToWeb(View view) {
        Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(urlLink));
        TextView textView = (TextView) findViewById(R.id.collection_title);
        textView.setTextColor(Color.GREEN);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EditText editText = (EditText) findViewById(R.id.query_edit2);
        editText.setVisibility(View.GONE);
        TextView textView = (TextView) findViewById(R.id.collection_title);
        textView.setTextColor(getResources().getColor(R.color.primary_text));
    }

    public void setAsWallpaper() throws IOException, URISyntaxException {
        //Send the picture
        URL url = new URL(Detailed.getWebUrl());
        Bitmap bmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        setImagePath(saveToInternalStorage(bmap));
        // setImagePath(SaveImage(bmap));
        File file = new ContextWrapper(getApplicationContext()).getDir("imageDir",Context.MODE_PRIVATE);
        //       Bitmap preBmp = Bitmap.createScaledBitmap
        //     (bmap2 , bmap2.getWidth()/2 , bmap2.getHeight()/2 , true);
        //TODO Debug for null pointer exception
        //     setImagePath(saveToInternalStorage(preBmp));
       Intent intent = new Intent(this,CropImageActivity.class);
        intent.putExtra("imagePath",getImagePath());
        startActivity(intent);
    }

    @NonNull
    private String saveToInternalStorage(Bitmap bitmapImage) throws IOException {
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        System.out.println(directory.toString());
        // Create imageDir
        File mypath=new File(directory,"profile.jpg");
        //TODO orbayin bahsettigi kucultme
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            fos.close();
        }
        return directory.getAbsolutePath();
    }


    @Override
    public void onClick(int buttonIndex) {
        switch (buttonIndex){
            case 0:
                try {
                    shareIt();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case 1:
                try {
                    attemptImageToDevice();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                break;
            case 2:
                try {
                    setAsWallpaper();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
