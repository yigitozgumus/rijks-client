package com.example.ozgumusy.rijksclient.network;

import com.example.ozgumusy.rijksclient.ArtContentDetailed;
import com.example.ozgumusy.rijksclient.jsonmodels.ArtContent;
import com.example.ozgumusy.rijksclient.jsonmodels.TileSet;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by ozgumusy on 21/07/2016.
 */
public interface CollectionAPI {
    //Create the ENDPoint
    String ENDPOINT_EN= "https://www.rijksmuseum.nl/api/en/";
    String ENDPOINT_NL="https://www.rijksmuseum.nl/api/nl/";


    Retrofit retrofitEn = new Retrofit.Builder()
            .baseUrl(CollectionAPI.ENDPOINT_EN)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    Retrofit retrofitNl = new Retrofit.Builder()
            .baseUrl(CollectionAPI.ENDPOINT_NL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    /*  Collection List Call
    *   Query Items:
    *   ps number
    *   key
    *   s (relevance)
    *   format
    * */
    @GET("collection")
    Call<ArtContent> getCollections(@QueryMap Map<String, String> options);

    /* Collection Details Call
    *  Query Items:
    *  key
    *  format
    *
    * */
    @GET("collection/{collectionID}")
    Call<ArtContentDetailed> getCollectionDetal(@Path("collectionID") String collectionID,
                                                @QueryMap Map<String, String> option);
    /* Collection Images Call
    *  Query Items
    *  key
    *  format
    *
    * */
    @GET("collection/{collectionID}/tiles")
    Call<TileSet> getTilesOfCollection(@Path("collectionID") String collectionID,
                                       @QueryMap Map<String, String> options);

}
