package com.example.ozgumusy.rijksclient.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.ozgumusy.rijksclient.R;
import com.example.ozgumusy.rijksclient.jsonmodels.ArtContent;
import com.example.ozgumusy.rijksclient.network.CollectionAPI;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;




public class MainActivity extends AppCompatActivity {

    //========================== Object Initializations ============================================
    //Message names
    public static final String COLLECTION_MESSAGE = "com.example.ozgumusy.rijksclient.MESSAGE";
    public static final String ID = "com.example.ozgumusy.rijksclient.MESSAGE";
    private static final int RESULT_SETTINGS = 1;
    private static final int LANGUAGE_SETTINGS = 2;
    public static final String ANONYMOUS = "anonymous";
    //String for query transfer
    private String collectionTarget = "";
    // Firebase instance variables
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private String mUsername;
    private String mPhotoUrl;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private int[] IMAGE_IDS = {
            R.drawable.mainp1, R.drawable.mainp2, R.drawable.mainp3,
            R.drawable.mainp4,R.drawable.mainp5,R.drawable.mainp6
    };
    private GoogleApiClient mGoogleApiClient;

    protected static Locale myLocale;
    protected static String languageDefault ="en" ;
    //AutocompleteText query set
    protected static ArrayList<String> queries ;
    //Objects for the views
    AutoCompleteTextView queryTextView;

    ImageView slidingimage;

    //Slide Image Initializations
    public int currentimageindex=0;


    public static void setResultNum(int resultNum) {
        MainActivity.resultNum = resultNum;
    }
    //Query Search number and Search Bar visibility
    protected static int resultNum ;
    private boolean isQueryVisible = false;

    public void setQueryVisible(boolean queryVisible) {
        isQueryVisible = queryVisible;
    }
    public static void setLanguageDefault(String languageDefault) {
        MainActivity.languageDefault = languageDefault;
    }
    //========================= Functions ==========================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Authentication Stuff
        mUsername = ANONYMOUS;

        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(this, SignInActivity.class));
            finish();
            return;
        } else {
            mUsername = mFirebaseUser.getDisplayName();
            if (mFirebaseUser.getPhotoUrl() != null) {
                mPhotoUrl = mFirebaseUser.getPhotoUrl().toString();
            }
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        queryTextView = (AutoCompleteTextView) findViewById(R.id.query_edit);
        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_expandable_list_item_1,queries);
        queryTextView.setAdapter(adapter);
        queryTextView.setThreshold(1);
        setResultNum(25);
        queries = new ArrayList<>();
        getInitialCollection();
        //Create the Menu
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void getInitialCollection(){
        //Set the main screen picture
        CollectionAPI colApi = CollectionAPI.retrofitEn.create(CollectionAPI.class);
        Map<String,String> data = new HashMap<>();
        data.put("ps",String.valueOf(100000));
        //data.put("q","van gogh");
        data.put("format","json");
        data.put("key","uvB5vrkP");
        // data.put("type","painting");
        Call<ArtContent> call = colApi.getCollections(data);
        System.out.println(call.request().url().toString());
        call.enqueue(new Callback<ArtContent>() {
            @Override
            public void onResponse(Call<ArtContent> call, Response<ArtContent> response) {
                if(response.isSuccessful()){
                    Gson sender = new Gson();
                    collectionTarget = sender.toJson(response.body());
                    for (int i = 0; i <response.body().getArtObjects().size() ; i++) {
                       queries.add(response.body().getArtObjects().get(i).getPrincipalOrFirstMaker());
                    }
                    Set<String> replacement = new HashSet<>();
                    replacement.addAll(queries);
                    queries.clear();
                    queries.addAll(replacement);
                }
            }
            @Override
            public void onFailure(Call<ArtContent> call, Throwable t) {
                System.out.println(t.getCause());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                // User chose the "Settings" item, show the app settings UI...
                Intent intent = new Intent(this,SettingsActivity.class);
                transferUserSettings();
                startActivityForResult(intent,RESULT_SETTINGS);
                return true;

            case R.id.action_search:
                EditText editText = (EditText) findViewById(R.id.query_edit);
                String query = editText.getText().toString();
                if(!isQueryVisible){
                    View myView = findViewById(R.id.query_edit);
                    int cx = myView.getWidth()/2;
                    int cy = myView.getHeight()/2;
                    float finalRadius = (float) Math.hypot(cx,cy);
                    Animator anim = ViewAnimationUtils.createCircularReveal(myView,cx,cy,0,finalRadius);
                    myView.setVisibility(View.VISIBLE);
                    anim.start();
                    setQueryVisible(true);
                }else if(isQueryVisible && query.equals("")){
                    View myView = findViewById(R.id.query_edit);
                    int cx = myView.getWidth()/2;
                    int cy = myView.getHeight()/2;
                    float initialRadius = (float) Math.hypot(cx,cy);
                    Animator anim = ViewAnimationUtils.createCircularReveal(myView,cx,cy,initialRadius,0);
                    anim.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            myView.setVisibility(View.INVISIBLE);
                        }
                    });
                    anim.start();
                    setQueryVisible(false);
                }else{
                    makeQuery(query);
                }
                return true;
            case R.id.sign_out_menu:
                mFirebaseAuth.signOut();
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                mUsername = ANONYMOUS;
                startActivity(new Intent(this, SignInActivity.class));
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    public void setLocale(String lang) {
        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        //TODO Learn its replacement
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, MainActivity.class);
        startActivity(refresh);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_SETTINGS:
                updateUserSettings();
                break;
        }

    }
    private void updateUserSettings() {
        SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        String results = sharedPrefs.getString("number_of_searches","NULL");
        String language = sharedPrefs.getString("language_choice","NULL");
        if (!language.equals(languageDefault)){
            setLanguageDefault(language);
            setLocale(language);
        }
        setResultNum(Integer.parseInt(results));

    }
    private void transferUserSettings() {
        SharedPreferences prefs = PreferenceManager.
                getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("number_of_searches", String.valueOf(resultNum));
        editor.putString("language_choice",languageDefault);
        editor.commit();
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    public void onClick(View view) {
        // Create the intent for the Collection Activity
        Intent intent = new Intent(this, CollectionsActivity.class);
        // Object Creation and the ps number extraction from the EditText
        // Send the ps Number to the CollectionsActivity
        intent.putExtra(ID,"collection");
        intent.putExtra(COLLECTION_MESSAGE,collectionTarget);
        startActivity(intent);
    }

    public void makeQuery(String query) {
        Intent intent = new Intent(this,CollectionsActivity.class);
        //Get the query Text
        final StringBuilder[] test = {new StringBuilder()};
        // Api Call Initializations
        CollectionAPI colApi = CollectionAPI.retrofitEn.create(CollectionAPI.class);
        Map<String,String> data = new HashMap<>();
        data.put("ps",String.valueOf(resultNum));
        data.put("q",query);
        data.put("format","json");
        data.put("key","uvB5vrkP");
        // data.put("type","painting");
        Call<ArtContent> callNew = colApi.getCollections(data);
        System.out.println(callNew.request().url().toString());
        callNew.enqueue(new Callback<ArtContent>() {
            @Override
            public void onResponse(Call<ArtContent> call, Response<ArtContent> response) {
                Gson gson = new Gson();
                test[0] = new StringBuilder(gson.toJson(response.body()).replace("\\u003ds0",""));
                //System.out.println("Main screen inside mq response "+ test[0]);
                //Get the query Text
                intent.putExtra(COLLECTION_MESSAGE, test[0].toString());
                startActivity(intent);

            }
            @Override
            public void onFailure(Call<ArtContent> call, Throwable t) {
                System.out.println(t.getCause());
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        EditText editText = (EditText) findViewById(R.id.query_edit);
        editText.setVisibility(View.GONE);
        setQueryVisible(false);
        editText.setText("");
        editText.setHint(getResources().getString(R.string.search));

    }

}