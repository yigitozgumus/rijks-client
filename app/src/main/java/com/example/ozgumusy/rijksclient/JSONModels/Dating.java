package com.example.ozgumusy.rijksclient.jsonmodels;

/**
 * Created by ozgumusy on 21/07/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Dating {

    @SerializedName("early")
    @Expose
    private Object early;
    @SerializedName("earlyPrecision")
    @Expose
    private Object earlyPrecision;
    @SerializedName("late")
    @Expose
    private Object late;
    @SerializedName("latePrecision")
    @Expose
    private Object latePrecision;
    @SerializedName("year")
    @Expose
    private Integer year;
    @SerializedName("yearEarly")
    @Expose
    private Integer yearEarly;
    @SerializedName("yearLate")
    @Expose
    private Integer yearLate;
    @SerializedName("period")
    @Expose
    private Integer period;

    /**
     *
     * @return
     * The early
     */
    public Object getEarly() {
        return early;
    }


    /**
     *
     * @return
     * The earlyPrecision
     */
    public Object getEarlyPrecision() {
        return earlyPrecision;
    }

    /**
     *
     * @return
     * The late
     */
    public Object getLate() {
        return late;
    }

    /**
     *
     * @return
     * The latePrecision
     */
    public Object getLatePrecision() {
        return latePrecision;
    }

    /**
     *
     * @return
     * The year
     */
    public Integer getYear() {
        return year;
    }

    /**
     *
     * @return
     * The yearEarly
     */
    public Integer getYearEarly() {
        return yearEarly;
    }

    /**
     *
     * @return
     * The yearLate
     */
    public Integer getYearLate() {
        return yearLate;
    }

    /**
     *
     * @return
     * The period
     */
    public Integer getPeriod() {
        return period;
    }

}
