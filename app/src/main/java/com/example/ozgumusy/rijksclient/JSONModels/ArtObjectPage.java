package com.example.ozgumusy.rijksclient.jsonmodels;

/**
 * Created by ozgumusy on 21/07/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class ArtObjectPage {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("similarPages")
    @Expose
    private List<Object> similarPages = new ArrayList<Object>();
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("objectNumber")
    @Expose
    private String objectNumber;
    @SerializedName("tags")
    @Expose
    private List<Object> tags = new ArrayList<Object>();
    @SerializedName("plaqueDescription")
    @Expose
    private Object plaqueDescription;
    @SerializedName("audioFile1")
    @Expose
    private Object audioFile1;
    @SerializedName("audioFileLabel1")
    @Expose
    private Object audioFileLabel1;
    @SerializedName("audioFileLabel2")
    @Expose
    private Object audioFileLabel2;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("updatedOn")
    @Expose
    private String updatedOn;
    @SerializedName("adlibOverrides")
    @Expose
    private AdlibOverrides adlibOverrides;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }


    /**
     *
     * @return
     * The similarPages
     */
    public List<Object> getSimilarPages() {
        return similarPages;
    }


    /**
     *
     * @return
     * The lang
     */
    public String getLang() {
        return lang;
    }


    /**
     *
     * @return
     * The objectNumber
     */
    public String getObjectNumber() {
        return objectNumber;
    }


    /**
     *
     * @return
     * The tags
     */
    public List<Object> getTags() {
        return tags;
    }


    /**
     *
     * @return
     * The plaqueDescription
     */
    public Object getPlaqueDescription() {
        return plaqueDescription;
    }

    /**
     *
     * @return
     * The audioFile1
     */
    public Object getAudioFile1() {
        return audioFile1;
    }


    /**
     *
     * @return
     * The audioFileLabel1
     */
    public Object getAudioFileLabel1() {
        return audioFileLabel1;
    }


    /**
     *
     * @return
     * The audioFileLabel2
     */
    public Object getAudioFileLabel2() {
        return audioFileLabel2;
    }

    /**
     *
     * @return
     * The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     *
     * @return
     * The updatedOn
     */
    public String getUpdatedOn() {
        return updatedOn;
    }

    /**
     *
     * @return
     * The adlibOverrides
     */
    public AdlibOverrides getAdlibOverrides() {
        return adlibOverrides;
    }


}
