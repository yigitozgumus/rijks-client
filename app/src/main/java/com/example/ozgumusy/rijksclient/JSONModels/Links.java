package com.example.ozgumusy.rijksclient.jsonmodels;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Links {

    @SerializedName("self")
    @Expose
    private String self;
    @SerializedName("web")
    @Expose
    private String web;

    /**
     *
     * @return
     * The self
     */
    public String getSelf() {
        return self;
    }

    /**
     *
     * @return
     * The web
     */
    public String getWeb() {
        return web;
    }

}