package com.example.ozgumusy.rijksclient.jsonmodels;

/**
 * Created by ozgumusy on 21/07/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Classification {

    @SerializedName("iconClassIdentifier")
    @Expose
    private List<String> iconClassIdentifier = new ArrayList<String>();
    @SerializedName("iconClassDescription")
    @Expose
    private List<String> iconClassDescription = new ArrayList<String>();
    @SerializedName("motifs")
    @Expose
    private List<Object> motifs = new ArrayList<Object>();
    @SerializedName("events")
    @Expose
    private List<Object> events = new ArrayList<Object>();
    @SerializedName("periods")
    @Expose
    private List<Object> periods = new ArrayList<Object>();
    @SerializedName("places")
    @Expose
    private List<String> places = new ArrayList<String>();
    @SerializedName("people")
    @Expose
    private List<String> people = new ArrayList<String>();
    @SerializedName("objectNumbers")
    @Expose
    private List<String> objectNumbers = new ArrayList<String>();

    /**
     *
     * @return
     * The iconClassIdentifier
     */
    public List<String> getIconClassIdentifier() {
        return iconClassIdentifier;
    }

    /**
     *
     * @return
     * The iconClassDescription
     */
    public List<String> getIconClassDescription() {
        return iconClassDescription;
    }


    /**
     *
     * @return
     * The motifs
     */
    public List<Object> getMotifs() {
        return motifs;
    }

    /**
     *
     * @return
     * The events
     */
    public List<Object> getEvents() {
        return events;
    }

    /**
     *
     * @return
     * The periods
     */
    public List<Object> getPeriods() {
        return periods;
    }

    /**
     *
     * @return
     * The places
     */
    public List<String> getPlaces() {
        return places;
    }

    /**
     *
     * @return
     * The people
     */
    public List<String> getPeople() {
        return people;
    }

    /**
     *
     * @return
     * The objectNumbers
     */
    public List<String> getObjectNumbers() {
        return objectNumbers;
    }

}
