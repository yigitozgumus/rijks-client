package com.example.ozgumusy.rijksclient.activities;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.example.ozgumusy.rijksclient.R;


public class SettingsActivity extends PreferenceActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
    }    }