package com.example.ozgumusy.rijksclient.jsonmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ThumbnailLandscape {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("url")
    @Expose
    private String url;

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

}

