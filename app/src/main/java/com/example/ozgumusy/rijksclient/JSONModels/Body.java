package com.example.ozgumusy.rijksclient.jsonmodels;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Body {

    @SerializedName("markdown")
    @Expose
    private String markdown;
    @SerializedName("html")
    @Expose
    private String html;

    /**
     *
     * @return
     * The markdown
     */
    public String getMarkdown() {
        return markdown;
    }

    /**
     *
     * @return
     * The html
     */
    public String getHtml() {
        return html;
    }

}