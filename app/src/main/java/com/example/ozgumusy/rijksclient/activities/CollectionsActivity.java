package com.example.ozgumusy.rijksclient.activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.example.ozgumusy.rijksclient.ArtCollection;
import com.example.ozgumusy.rijksclient.CollectionAdapter;
import com.example.ozgumusy.rijksclient.R;
import com.example.ozgumusy.rijksclient.jsonmodels.ArtContent;
import com.example.ozgumusy.rijksclient.jsonmodels.ArtObject;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class CollectionsActivity extends AppCompatActivity {

    public static final String OBJECT_MESSAGE = "com.example.ozgumusy.rijksclient.MESSAGE";
    static final String COLLECTION_STATE = "collectionState";
    protected ArtContent Collections ;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager layoutManager;
    private CollectionAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection);
        //Get the data from the API =======================
        //Get the intent
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        Gson converter = new Gson();
        if(Collections == null){
            Collections = converter
                    .fromJson(extras.getString(MainActivity.COLLECTION_MESSAGE),ArtContent.class);
        }
        initViews();
        //===============================================
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(COLLECTION_STATE,new Gson().toJson(Collections));

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Collections = new Gson().fromJson(savedInstanceState.getString(COLLECTION_STATE),ArtContent.class);
    }

    private ArrayList getData (){
        ArrayList<ArtCollection> art_col = new ArrayList<>();
        List<ArtObject> artObjList = Collections.getArtObjects();
        for (int i = 0; i <artObjList.size() ; i++) {
            ArtCollection artInd = new ArtCollection();
            artInd.setId(artObjList.get(i).getId());
            artInd.setObjectNum(artObjList.get(i).getObjectNumber());
            artInd.setTitle(artObjList.get(i).getTitle());
            if(artObjList.get(i).getWebImage() != null)
                artInd.setWebImageUrl(artObjList.get(i).getWebImage().getUrl());
            art_col.add(artInd);
        }
        return art_col;
    }

    private void initViews(){
        mRecyclerView = (RecyclerView) findViewById(R.id.Recycler_view);
        mRecyclerView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(getApplicationContext(),1);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        ArrayList artCollections = getData();
        adapter = new CollectionAdapter(getApplicationContext(),artCollections);
        adapter.setOnItemClickListener(new CollectionAdapter.ClickListener(){
            @Override
            public void onItemClick(int position, View v) {
                Intent intent = new Intent(v.getContext(),CollectionPageActivity.class);
                intent.putExtra("link",Collections
                                        .getArtObjects()
                                        .get(position)
                                        .getLinks()
                                        .getWeb());
                ImageView imageView =(ImageView) findViewById(R.id.collection_img);
                ActivityOptions options = ActivityOptions
                        .makeSceneTransitionAnimation(CollectionsActivity.this,
                                                                (View)imageView,"imageHero");
                intent.putExtra(OBJECT_MESSAGE,Collections
                        .getArtObjects()
                        .get(position)
                        .getObjectNumber());
                startActivity(intent,options.toBundle());
            }
            @Override
            public void onItemLongClick(int position, View v) {
                //TODO implementation absentß
            }
        });
        mRecyclerView.setAdapter(adapter);
    }
}
