package com.example.ozgumusy.rijksclient.jsonmodels;

/**
 * Created by ozgumusy on 26/07/2016.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class TileSet {

    @SerializedName("levels")
    @Expose
    private List<Level> levels = new ArrayList<Level>();

    /**
     *
     * @return
     * The levels
     */
    public List<Level> getLevels() {
        return levels;
    }

}