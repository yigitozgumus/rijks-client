package com.example.ozgumusy.rijksclient;

import com.example.ozgumusy.rijksclient.jsonmodels.ColorsWithNormalization;
import com.example.ozgumusy.rijksclient.jsonmodels.PrincipalMaker;

import java.util.List;

/**
 * Created by ozgumusy on 27/07/2016.
 */
public class ArtCollectionDetail {

    //Data Fields for the Collection Details
    private String title ;
    private String webUrl ;
    private String collectionURL;
    private List<Object> colorPalette ;
    private List<String> titles;
    private Object description;
    private List<PrincipalMaker> principalMakers;
    private List<ColorsWithNormalization> normalized32Colors;

    public List<ColorsWithNormalization> getNormalized32Colors() {
        return normalized32Colors;
    }

    public void setNormalized32Colors(List<ColorsWithNormalization> normalized32Colors) {
        this.normalized32Colors = normalized32Colors;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public List<Object> getColorPalette() {
        return colorPalette;
    }

    public void setColorPalette(List<Object> colorPalette) {
        this.colorPalette = colorPalette;
    }

    public List<String> getTitles() {
        return titles;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public List<PrincipalMaker> getPrincipalMaker() {
        return principalMakers;
    }

    public void setPrincipalMaker(List<PrincipalMaker> principalMaker) {
        this.principalMakers = principalMaker;
    }

    public String getCollectionURL() {
        return collectionURL;
    }

    public void setCollectionURL(String collectionURL) {
        this.collectionURL = collectionURL;
    }

}
