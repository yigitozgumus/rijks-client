package com.example.ozgumusy.rijksclient.jsonmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Level {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("width")
    @Expose
    private Integer width;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("tiles")
    @Expose
    private List<Tile> tiles = new ArrayList<Tile>();

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return
     * The width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     *
     * @return
     * The height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     *
     * @return
     * The tiles
     */
    public List<Tile> getTiles() {
        return tiles;
    }

}