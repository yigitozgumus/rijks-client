package com.example.ozgumusy.rijksclient;

import com.example.ozgumusy.rijksclient.jsonmodels.ArtObjectDetailed;
import com.example.ozgumusy.rijksclient.jsonmodels.ArtObjectPage;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ozgumusy on 27/07/2016.
 */
public class ArtContentDetailed {

    @SerializedName("elapsedMilliseconds")
    @Expose
    private Integer elapsedMilliseconds;


    @SerializedName("artObject")
    @Expose
    private ArtObjectDetailed artObject ;

    @SerializedName("artObjectPage")
    @Expose
    private ArtObjectPage artObjectPage;

    public Integer getElapsedMilliseconds() {
        return elapsedMilliseconds;
    }

    public ArtObjectPage getArtObjectPage() {
        return artObjectPage;
    }


    public ArtObjectDetailed getArtObject() {
        return artObject;
    }

}
