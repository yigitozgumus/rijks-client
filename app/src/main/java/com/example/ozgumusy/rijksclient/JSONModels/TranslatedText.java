package com.example.ozgumusy.rijksclient.jsonmodels;

/**
 * Created by ozgumusy on 27/07/2016.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class TranslatedText {

    @SerializedName("data")
    @Expose
    private Data data;

    /**
     *
     * @return
     * The data
     */
    public Data getData() {
        return data;
    }

}
