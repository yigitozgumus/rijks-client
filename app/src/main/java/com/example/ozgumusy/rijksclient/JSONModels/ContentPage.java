package com.example.ozgumusy.rijksclient.jsonmodels;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ContentPage {

    @SerializedName("elapsedMilliseconds")
    @Expose
    private Integer elapsedMilliseconds;
    @SerializedName("contentPage")
    @Expose
    private ContentPage_ contentPage;
    @SerializedName("similarPages")
    @Expose
    private Object similarPages;

    /**
     *
     * @return
     * The elapsedMilliseconds
     */
    public Integer getElapsedMilliseconds() {
        return elapsedMilliseconds;
    }


    /**
     *
     * @return
     * The contentPage
     */
    public ContentPage_ getContentPage() {
        return contentPage;
    }

    /**
     *
     * @return
     * The similarPages
     */
    public Object getSimilarPages() {
        return similarPages;
    }

}