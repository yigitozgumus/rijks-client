package com.example.ozgumusy.rijksclient.jsonmodels;

/**
 * Created by ozgumusy on 21/07/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Dimension {

    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("part")
    @Expose
    private Object part;
    @SerializedName("value")
    @Expose
    private String value;

    /**
     *
     * @return
     * The unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @return
     * The part
     */
    public Object getPart() {
        return part;
    }

    /**
     *
     * @return
     * The value
     */
    public String getValue() {
        return value;
    }

}
