package com.example.ozgumusy.rijksclient.jsonmodels;

/**
 * Created by ozgumusy on 28/07/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Data {

    @SerializedName("translations")
    @Expose
    private List<Translation> translations = new ArrayList<Translation>();

    /**
     *
     * @return
     * The translations
     */
    public List<Translation> getTranslations() {
        return translations;
    }


}