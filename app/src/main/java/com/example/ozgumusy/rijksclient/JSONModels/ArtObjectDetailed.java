package com.example.ozgumusy.rijksclient.jsonmodels;

/**
 * Created by ozgumusy on 27/07/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class ArtObjectDetailed {

    @SerializedName("links")
    @Expose
    private Links links;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("priref")
    @Expose
    private String priref;
    @SerializedName("objectNumber")
    @Expose
    private String objectNumber;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("copyrightHolder")
    @Expose
    private Object copyrightHolder;
    @SerializedName("webImage")
    @Expose
    private WebImage webImage;
    @SerializedName("colors")
    @Expose
    private List<Object> colors = new ArrayList<Object>();
    @SerializedName("colorsWithNormalization")
    @Expose
    private List<ColorsWithNormalization> colorsWithNormalization = new ArrayList<>();
    @SerializedName("normalizedColors")
    @Expose
    private List<Object> normalizedColors = new ArrayList<Object>();
    @SerializedName("normalized32Colors")
    @Expose
    private List<Object> normalized32Colors = new ArrayList<Object>();
    @SerializedName("titles")
    @Expose
    private List<String> titles = new ArrayList<String>();
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("labelText")
    @Expose
    private Object labelText;
    @SerializedName("objectTypes")
    @Expose
    private List<String> objectTypes = new ArrayList<String>();
    @SerializedName("objectCollection")
    @Expose
    private List<Object> objectCollection = new ArrayList<Object>();
    @SerializedName("makers")
    @Expose
    private List<Maker> makers = new ArrayList<Maker>();
    @SerializedName("principalMakers")
    @Expose
    private List<PrincipalMaker> principalMakers = new ArrayList<PrincipalMaker>();
    @SerializedName("plaqueDescriptionDutch")
    @Expose
    private Object plaqueDescriptionDutch;
    @SerializedName("plaqueDescriptionEnglish")
    @Expose
    private Object plaqueDescriptionEnglish;
    @SerializedName("principalMaker")
    @Expose
    private String principalMaker;
    @SerializedName("artistRole")
    @Expose
    private Object artistRole;
    @SerializedName("associations")
    @Expose
    private List<Object> associations = new ArrayList<Object>();
    @SerializedName("acquisition")
    @Expose
    private Acquisition acquisition;
    @SerializedName("exhibitions")
    @Expose
    private List<Object> exhibitions = new ArrayList<Object>();
    @SerializedName("materials")
    @Expose
    private List<String> materials = new ArrayList<String>();
    @SerializedName("techniques")
    @Expose
    private List<String> techniques = new ArrayList<String>();
    @SerializedName("productionPlaces")
    @Expose
    private List<String> productionPlaces = new ArrayList<String>();
    @SerializedName("dating")
    @Expose
    private Dating dating;
    @SerializedName("classification")
    @Expose
    private Classification classification;
    @SerializedName("hasImage")
    @Expose
    private Boolean hasImage;
    @SerializedName("historicalPersons")
    @Expose
    private List<Object> historicalPersons = new ArrayList<Object>();
    @SerializedName("inscriptions")
    @Expose
    private List<Object> inscriptions = new ArrayList<Object>();
    @SerializedName("documentation")
    @Expose
    private List<String> documentation = new ArrayList<String>();
    @SerializedName("catRefRPK")
    @Expose
    private List<String> catRefRPK = new ArrayList<String>();
    @SerializedName("principalOrFirstMaker")
    @Expose
    private String principalOrFirstMaker;
    @SerializedName("dimensions")
    @Expose
    private List<Dimension> dimensions = new ArrayList<Dimension>();
    @SerializedName("physicalProperties")
    @Expose
    private List<Object> physicalProperties = new ArrayList<Object>();
    @SerializedName("physicalMedium")
    @Expose
    private String physicalMedium;
    @SerializedName("longTitle")
    @Expose
    private String longTitle;
    @SerializedName("subTitle")
    @Expose
    private String subTitle;
    @SerializedName("scLabelLine")
    @Expose
    private String scLabelLine;
    @SerializedName("label")
    @Expose
    private Label label;
    @SerializedName("showImage")
    @Expose
    private Boolean showImage;
    @SerializedName("location")
    @Expose
    private Object location;

    /**
     *
     * @return
     * The links
     */
    public Links getLinks() {
        return links;
    }

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @return
     * The priref
     */
    public String getPriref() {
        return priref;
    }

    /**
     *
     * @return
     * The objectNumber
     */
    public String getObjectNumber() {
        return objectNumber;
    }

    /**
     *
     * @return
     * The language
     */
    public String getLanguage() {
        return language;
    }


    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }


    /**
     *
     * @return
     * The copyrightHolder
     */
    public Object getCopyrightHolder() {
        return copyrightHolder;
    }


    /**
     *
     * @return
     * The webImage
     */
    public WebImage getWebImage() {
        return webImage;
    }

    /**
     *
     * @return
     * The colors
     */
    public List<Object> getColors() {
        return colors;
    }

    /**
     *
     * @return
     * The colorsWithNormalization
     */
    public List<ColorsWithNormalization> getColorsWithNormalization() {
        return colorsWithNormalization;
    }


    /**
     *
     * @return
     * The normalizedColors
     */
    public List<Object> getNormalizedColors() {
        return normalizedColors;
    }


    /**
     *
     * @return
     * The normalized32Colors
     */
    public List<Object> getNormalized32Colors() {
        return normalized32Colors;
    }


    /**
     *
     * @return
     * The titles
     */
    public List<String> getTitles() {
        return titles;
    }

    /**
     *
     * @return
     * The description
     */
    public Object getDescription() {
        return description;
    }

    /**
     *
     * @return
     * The labelText
     */
    public Object getLabelText() {
        return labelText;
    }


    /**
     *
     * @return
     * The objectTypes
     */
    public List<String> getObjectTypes() {
        return objectTypes;
    }


    /**
     *
     * @return
     * The objectCollection
     */
    public List<Object> getObjectCollection() {
        return objectCollection;
    }


    /**
     *
     * @return
     * The makers
     */
    public List<Maker> getMakers() {
        return makers;
    }

    /**
     *
     * @return
     * The principalMakers
     */
    public List<PrincipalMaker> getPrincipalMakers() {
        return principalMakers;
    }



    /**
     *
     * @return
     * The plaqueDescriptionDutch
     */
    public Object getPlaqueDescriptionDutch() {
        return plaqueDescriptionDutch;
    }


    /**
     *
     * @return
     * The plaqueDescriptionEnglish
     */
    public Object getPlaqueDescriptionEnglish() {
        return plaqueDescriptionEnglish;
    }

    /**
     *
     * @return
     * The principalMaker
     */
    public String getPrincipalMaker() {
        return principalMaker;
    }

    /**
     *
     * @return
     * The artistRole
     */
    public Object getArtistRole() {
        return artistRole;
    }


    /**
     *
     * @return
     * The associations
     */
    public List<Object> getAssociations() {
        return associations;
    }


    /**
     *
     * @return
     * The acquisition
     */
    public Acquisition getAcquisition() {
        return acquisition;
    }


    /**
     *
     * @return
     * The exhibitions
     */
    public List<Object> getExhibitions() {
        return exhibitions;
    }


    /**
     *
     * @return
     * The materials
     */
    public List<String> getMaterials() {
        return materials;
    }


    /**
     *
     * @return
     * The techniques
     */
    public List<String> getTechniques() {
        return techniques;
    }


    /**
     *
     * @return
     * The productionPlaces
     */
    public List<String> getProductionPlaces() {
        return productionPlaces;
    }


    /**
     *
     * @return
     * The dating
     */
    public Dating getDating() {
        return dating;
    }


    /**
     *
     * @return
     * The classification
     */
    public Classification getClassification() {
        return classification;
    }


    /**
     *
     * @return
     * The hasImage
     */
    public Boolean getHasImage() {
        return hasImage;
    }

    /**
     *
     * @return
     * The historicalPersons
     */
    public List<Object> getHistoricalPersons() {
        return historicalPersons;
    }

    /**
     *
     * @return
     * The inscriptions
     */
    public List<Object> getInscriptions() {
        return inscriptions;
    }

    /**
     *
     * @return
     * The documentation
     */
    public List<String> getDocumentation() {
        return documentation;
    }


    /**
     *
     * @return
     * The catRefRPK
     */
    public List<String> getCatRefRPK() {
        return catRefRPK;
    }

    /**
     *
     * @return
     * The principalOrFirstMaker
     */
    public String getPrincipalOrFirstMaker() {
        return principalOrFirstMaker;
    }


    /**
     *
     * @return
     * The dimensions
     */
    public List<Dimension> getDimensions() {
        return dimensions;
    }

    /**
     *
     * @return
     * The physicalProperties
     */
    public List<Object> getPhysicalProperties() {
        return physicalProperties;
    }



    /**
     *
     * @return
     * The physicalMedium
     */
    public String getPhysicalMedium() {
        return physicalMedium;
    }

    /**
     *
     * @return
     * The longTitle
     */
    public String getLongTitle() {
        return longTitle;
    }

    /**
     *
     * @return
     * The subTitle
     */
    public String getSubTitle() {
        return subTitle;
    }

    /**
     *
     * @return
     * The scLabelLine
     */
    public String getScLabelLine() {
        return scLabelLine;
    }

    /**
     *
     * @return
     * The label
     */
    public Label getLabel() {
        return label;
    }

    /**
     *
     * @return
     * The showImage
     */
    public Boolean getShowImage() {
        return showImage;
    }


    /**
     *
     * @return
     * The location
     */
    public Object getLocation() {
        return location;
    }

}