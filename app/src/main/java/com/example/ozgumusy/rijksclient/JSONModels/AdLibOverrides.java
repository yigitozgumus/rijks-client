package com.example.ozgumusy.rijksclient.jsonmodels;

/**
 * Created by ozgumusy on 21/07/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class AdlibOverrides {

    @SerializedName("titel")
    @Expose
    private Object titel;
    @SerializedName("maker")
    @Expose
    private Object maker;
    @SerializedName("etiketText")
    @Expose
    private Object etiketText;

    /**
     *
     * @return
     * The titel
     */
    public Object getTitel() {
        return titel;
    }


    /**
     *
     * @return
     * The maker
     */
    public Object getMaker() {
        return maker;
    }



    /**
     *
     * @return
     * The etiketText
     */
    public Object getEtiketText() {
        return etiketText;
    }


}
